﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EVT.MVVMSupport
{
    public interface IView
    {
        Object DataContext { get; set; }
        void Activate();
        event RoutedEventHandler Loaded;
        event RoutedEventHandler Unloaded;
    }
}
