﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EVT.MVVMSupport
{
    public interface IDelegateCommand
    {
        void RaiseCanExecuteChanged();
    }
}
