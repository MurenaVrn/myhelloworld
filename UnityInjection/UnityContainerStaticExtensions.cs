﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace EVT.MicrosoftUnity.UnityContainerExtentions
{
    public static class UnityContainerStaticExtensions
    {
        public static void RegisterType<T>(this IUnityContainer container, InstanceLifeTime instanceLifeTime)
        {
            container.RegisterType<T>(GetLifeTimeManager(instanceLifeTime));
        }

        public static void RegisterType<TFrom, TTo>(this IUnityContainer container, InstanceLifeTime instanceLifeTime) where TTo:TFrom
        {
            container.RegisterType<TFrom, TTo>(GetLifeTimeManager(instanceLifeTime));
        }
        private static LifetimeManager GetLifeTimeManager(InstanceLifeTime instanceLifeTime)
        {
            switch (instanceLifeTime)
            {
                case InstanceLifeTime.Singleton:
                    return new ContainerControlledLifetimeManager();
                case InstanceLifeTime.Transient:
                    return new TransientLifetimeManager();
                default:
                    return new PerThreadLifetimeManager();
            }
        }



        
    }
}
