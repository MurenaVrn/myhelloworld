﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.MicrosoftUnity.UnityContainerExtentions
{
    public enum InstanceLifeTime: Byte
    {
        Singleton,
        Transient
    }
}
