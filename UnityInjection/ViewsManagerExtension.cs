﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.Unity;
using EVT.MVVMSupport;
using EVT.MHWApplication.Windows;


namespace EVT.MicrosoftUnity.UnityContainerExtentions
{ 
    class ViewsManagerExtension : UnityContainerExtension
    {
        private readonly Dictionary<EnumViewTypes, Type> mapping = new Dictionary<EnumViewTypes, Type>();
        
        public ViewsManagerExtension() { //Конструктор вызывается при добавлении расширения с помощью метода AddExtention
            //Manual configuration : writing into dictionary mapping
            mapping[EnumViewTypes.MainWindow]= typeof(MainWindow);
        }
        protected override void Initialize()
        {
            Container.RegisterType<IViewsService, ViewManager>(InstanceLifeTime.Singleton);
            Container.RegisterInstance(new EventHandler<ViewTypeEventArgs>(ViewResolve));
            
        }
       
        private void ViewResolve(Object sender, ViewTypeEventArgs e)
        {
            var service = (IViewsService)sender;
            //получить тип окна
            Type t = mapping[e.ViewType];
            var view = Container.Resolve(t);
            e.View = view as IView; //для класса Window и его наследников даст null , так как они не реализуют IView явно
            if (view!=null && e.View==null)
            {
                // Отображение не будет записано в менджер, но будет существовать
                MessageBox.Show("Вид потерялся и манагер его не отслеживает");
            }
        }


    }
}
