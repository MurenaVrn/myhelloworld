﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using EVT.MicrosoftUnity.UnityContainerExtentions;

namespace UnityInjection
{
    public static class ContainerInjection
    {
        private static void SetConfiguration(IUnityContainer container)
        {
            //Manual UnityConfiguration-регистация типов и определение времени существования объектов
            
        }
        private static void AddExtensions(IUnityContainer container)
        {
            container.AddExtension(new ViewsManagerExtension());
        }
        public static void ApplicationRunByContainer<TMainWindow, TApp>() where TMainWindow : Window, new()
                                                                                  where TApp : Application
        {
            //var mw = new TMainWindow();
            using (var container = new UnityContainer())
            { 
                container.RegisterType<TMainWindow>(InstanceLifeTime.Singleton);
            // два варианта - либо вручную создавать гл окно и гл приложение и регать инстансы, либо регать классы и резовить их
            // вариант 2:
                var mw = container.Resolve<TMainWindow>() ;// по идее можно убрать и в методе ран вызвать резолв
            
                container.RegisterType<TApp>(InstanceLifeTime.Singleton);
                SetConfiguration(container);
                AddExtensions(container);
                container.Resolve<TApp>().Run(mw);
            }
        }
    }
}
