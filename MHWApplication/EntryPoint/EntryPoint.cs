﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityInjection;
using EVT.MHWApplication.Windows;


namespace EVT.MHWApplication
{
    class EntryPoint
    {
        [STAThread]
        public static void Main(String[] args)
        {
            ContainerInjection.ApplicationRunByContainer<MainWindow, MainApp>();
        }
    }
}
