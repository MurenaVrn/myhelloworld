﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using EVT.MVVMSupport;

namespace EVT.MHWApplication.Windows
{
    public class MHWWindowBase: Window, IView
    {
        Object IView.DataContext { get { return DataContext; } set { DataContext = value; } }
        void IView.Activate()
        {
            this.Activate();
        }
    }
    public class MHWPageBase : Page, IView
    {
        Object IView.DataContext { get { return DataContext; } set { DataContext = value; } }
        public void Activate()
        {
            NavigationService.GetNavigationService(this).Navigate(this);            
        }
    }
}
