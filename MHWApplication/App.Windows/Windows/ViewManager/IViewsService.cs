﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.MVVMSupport;

namespace EVT.MHWApplication.Windows
{
    public interface IViewsService
    {
        IView Current { get;  }
        Dictionary<EnumViewTypes,IList<IView>> Views { get; }
        //void SetCurrent(IView view);
        
        void AddNewView(EnumViewTypes viewType);
        void DeleteView(IView view);
        void NextView();
        void PreviousView();
        
    }
}
