﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.MVVMSupport;

namespace EVT.MHWApplication.Windows
{
    public class ViewTypeEventArgs: EventArgs
    {
        public ViewTypeEventArgs(EnumViewTypes viewType)
        { ViewType = viewType;
          View = null;
        }
        public EnumViewTypes ViewType { get; }
        public IView View { get; set; }
    }
}
