﻿using System;
using System.Collections.Generic;
using EVT.MVVMSupport;
using System.Windows;

namespace EVT.MHWApplication.Windows
{
    public class ViewManager: IViewsService
    {
        public ViewManager(EventHandler<ViewTypeEventArgs> viewGettingAction ) // Затребует в конструктор mainView
        {
            // mainWindow уже должен быть создан для запуска WPF приложения
            var viewType = EnumViewTypes.MainWindow;
            ViewGeting = viewGettingAction;
            AddNewView(viewType);
        }
        IView current; // устанавливается только из SetCurrent
        public IView Current { get { return current; }}
        private EnumViewTypes CurrentViewType { get; set; }
        Dictionary<EnumViewTypes, IList<IView>> views = new Dictionary<EnumViewTypes,IList<IView>>();
        public Dictionary<EnumViewTypes, IList<IView>> Views { get { return views; } }
        
        enum EnumNavigateType
        {
            main,
            first,
            next, 
            previous,
            last
        }
        void NavigateToView(IView view, EnumViewTypes vt)
        {
            CurrentViewType = vt;
            current = view;
            // выполнить активацию окна
            view.Activate(); OnCurrentViewChanged(new ViewTypeEventArgs(vt));
        }
        event EventHandler<ViewTypeEventArgs> CurrentViewChanged;
        void OnCurrentViewChanged(ViewTypeEventArgs e)
        { }
   
        
        public void AddNewView(EnumViewTypes viewType)
        {
            IView view;
            if (TryGetViewByContainer(viewType, out view))  //запустить метод, делегирующий создание вида контейнеру
            {
                IList<IView> list;
                if (!views.TryGetValue(viewType, out list)) list = new List<IView>(new IView[] { view });
                else
                {
                    if (!list.Contains(view)) list.Add(view);  
                }
                NavigateToView(view, viewType);
            }
        }
        public void DeleteView(IView view) {
        //должен работать автоматически при управлении через навигатионсервис
        // должен заставить контейнер удалить объект
                }
        public void NextView()
        {
            //только для мастеров

        }
        public void PreviousView()
        { }
   
        event EventHandler<ViewTypeEventArgs> ViewGeting;
        protected virtual void OnViewGeting(ViewTypeEventArgs e) // Запрос на вывод окна заданного типа
        {
            if (ViewGeting!=null) ViewGeting(this, e); //нет поддержки потокобезопасности
        }

        private Boolean TryGetViewByContainer(EnumViewTypes viewType, out IView view)
        {// Добавит, если запрашиваемый тип не зарегистрирован как синглтон иначе - вернет существующий
            Boolean result = false;
            view = null;
            var e = new ViewTypeEventArgs(viewType);
            OnViewGeting(e);// вернулась ссылка либо на новый экз-р либо на существующий , либо null
            if (e.View != null) { view = e.View; result = true; }
            return result;
        }
    }
}
