﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Input;
using EVT.MVVMSupport;

namespace EVT.MHWApplication.ViewModels
{
    public class TreeViewPageViewModel : BindableBase
    {
        //для привязки файла View необходимы свойства с поддержкой соответсвующих полей:
        //Double someField;
        //public Double SomeField { get { return someField; } set { if (SetProperty<Double>(ref someField, value, "SomeField")) return; } }

        //для использования в качестве элемента замль необходим открытый конструктор:
        public TreeViewPageViewModel()
        {
            //если есть свойства для привязки команд , то в конструкторе нужно присоединить соответствующие значения
            //ExitApplicationCommand = new DelegateCommand(ExecuteProc);
            AddCommand = new DelegateCommand(str =>
            {
                //проверка на валидность ввода - обязанность VM
                //int ival;
                //if (int.TryParse(str, out ival)) _model.AddValue(ival); - добавление значения в МОДЕЛЬ
                // Пока нет присоединённой модели
                Collect.Add(new StringWrapper(str.ToString()));
            });

            RemoveCommand = new DelegateCommand(item =>
            {
                //if (i.HasValue) _model.RemoveValue(i.Value);
                Collect.Remove((StringWrapper)item);
            });
            ShowMessage = new DelegateCommand(message => 
            {
                System.Windows.Forms.MessageBox.Show(message.ToString());
            });

            collect.Add(new StringWrapper("First step"));
            collect.Add(new StringWrapper("Second step"));
            collect.Add(new StringWrapper("Third step"));
        }
        public ICommand ExitApplicationCommand { get; protected set; }
        //private void ExecuteProc(Object param) // param считывается из CommandParametr<
        //{
        //}
        public ICommand AddCommand { get; }
        public ICommand RemoveCommand { get; }
        public ICommand ShowMessage { get; }

        private ObservableCollection<StringWrapper> collect = new ObservableCollection<StringWrapper>();
        public ObservableCollection<StringWrapper> Collect { get { return collect; } set { if (SetProperty<ObservableCollection<StringWrapper>>(ref collect, value, "Collect")) return; } }

        private StringWrapper selItem;
        public StringWrapper SelItem
        {
            get { return selItem; }
            set { if (SetProperty<StringWrapper>(ref selItem, value, "SelItem")) return; }
        }
    }
   
}

