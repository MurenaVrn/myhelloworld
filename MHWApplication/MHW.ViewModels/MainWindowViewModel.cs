﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using EVT.MVVMSupport;

namespace EVT.MHWApplication.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        //для привязки файла View необходимы свойства с поддержкой соответсвующих полей:
        //Double someField;
        //public Double SomeField { get { return someField; } set { if (SetProperty<Double>(ref someField, value, "SomeField")) return; } }

        //для использования в качестве элемента замль необходим открытый конструктор:
        public MainWindowViewModel()
        {
            //если есть свойства для привязки команд , то в конструкторе нужно присоединить соответствующие значения
            //ExitApplicationCommand = new DelegateCommand(ExecuteProc);
            AddCommand = new DelegateCommand(str =>
              {
                //проверка на валидность ввода - обязанность VM
                //int ival;
                //if (int.TryParse(str, out ival)) _model.AddValue(ival); - добавление значения в МОДЕЛЬ
                // Пока нет присоединённой модели
                Collect.Add(new StringWrapper(str.ToString()));
              });

            RemoveCommand = new DelegateCommand(item =>
            {
                //if (i.HasValue) _model.RemoveValue(i.Value);
               collect.Remove((StringWrapper)item);
            });

            collect.Add(new StringWrapper("Гагарин"));
            collect.Add(new StringWrapper("Титов"));
            collect.Add(new StringWrapper("Леонов"));
        }
        public IDelegateCommand ExitApplicationCommand { get; protected set; }
        //private void ExecuteProc(Object param) // param считывается из CommandParametr<
        //{
        //}
        public IDelegateCommand AddCommand { get; }
        public IDelegateCommand RemoveCommand { get; }

        private ObservableCollection<StringWrapper> collect = new ObservableCollection<StringWrapper>();
        public ObservableCollection<StringWrapper> Collect { get { return collect; } set { if (SetProperty<ObservableCollection<StringWrapper>>(ref collect, value, "Collect")) return; } }

        private StringWrapper selItem;
        public StringWrapper SelItem
        {
            get { return selItem; }
            set { if (SetProperty<StringWrapper>(ref selItem, value, "SelItem")) return; }
        }
    }

    public class StringWrapper
    {
        public String Text { get; set; }
        public ObservableCollection<StringWrapper> Collect { get; set; }
        public StringWrapper(String text)
        {   Text = text;
            Collect = new ObservableCollection<StringWrapper>();

            string[] split = text.Split(new Char[] {' '});

            if (split.Length > 1)
            {for (int i = 0; i < split.Length; i++)
                {if (split[i].Trim() != "")
                        Collect.Add(new StringWrapper(split[i]));} }
            else
                if (text.Length>1)
                    for (Int32 i = 0; i < text.Length; i++)
                    {
                        if (!Char.IsWhiteSpace(text[i]))
                            Collect.Add(new StringWrapper(text[i].ToString()));
                    }
       }
    }
}
